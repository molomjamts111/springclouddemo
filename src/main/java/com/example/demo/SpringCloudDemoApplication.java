package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class SpringCloudDemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringCloudDemoApplication.class, args);

    List<String> l = List.of("name", "age", "address", "another Value", "one more");

    l.stream().forEach(System.out::println);
  }
}
